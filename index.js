const http = require('http');
const https = require('https');
const url = require('url');
const cron = require('node-cron')
const fs = require('fs-extra')
const axios = require('axios')
const fo = require('fs')

let cache = {}
const EXPIRE_TIME = 60000
const CancelToken = axios.CancelToken

const focusOn = ['Clash.yaml','https','long']

let now;
let total;
let svgCache;
preLoad();
axios.interceptors.request.use(config => {
    // 如果需要缓存--考虑到并不是所有接口都需要缓存的情况
    if (config.cache) {
        let source = CancelToken.source()
        config.cancelToken = source.token
        // 去缓存池获取缓存数据
        let data = cache[config.url]
        // 获取当前时间戳
        let expire_time = getExpireTime()
        // 判断缓存池中是否存在已有数据 存在的话 再判断是否过期
        // 未过期 source.cancel会取消当前的请求 并将内容返回到拦截器的err中
        if (data && expire_time - data.expire < EXPIRE_TIME) {
            source.cancel(data)
        }
    }
    return config
})

// 响应拦截器中用于缓存数据 如果数据没有缓存的话
axios.interceptors.response.use(
    response => {
        // 只缓存get请求
        if (response.config.method === 'get' && response.config.cache) {
            // 缓存数据 并将当前时间存入 方便之后判断是否过期
            let data = {
                expire: getExpireTime(),
                data: response
            }
            cache[`${response.config.url}`] = data
            fs.writeJson('./cache.json',cache,(err)=>{
                if(err){console.log(`致命性错误，无法写入cache,${JSON.stringify(err)}`)}
        
            });
        }
        return response
    },
    error => {
        // 请求拦截器中的source.cancel会将内容发送到error中
        // 通过axios.isCancel(error)来判断是否返回有数据 有的话直接返回给用户
        if (axios.isCancel(error)) return Promise.resolve(error.message.data)
        // 如果没有的话 则是正常的接口错误 直接返回错误信息给用户
        return Promise.reject(error)
    }
)

// 获取当前时间
function getExpireTime() {
    return new Date().getTime()
}

setInterval(()=>{
    console.log(JSON.stringify(total))
    console.log(JSON.stringify(now))
},60000)
setInterval(()=>{
    fs.writeJson('./today.json',now,(err)=>{
        if(err){console.log(`致命性错误，无法写入today,${JSON.stringify(err)}`)}
    });
    fs.writeJson('./total.json',total,(err)=>{
        if(err){console.log(`致命性错误，无法写入list,${JSON.stringify(err)}`)}

    });
},500)

setInterval(()=>{
    for(let i=0;i<focusOn.length;i++){
        axios("https://raw.githubusercontent.com/yu-steven/openit/main/"+focusOn[i],{cache: true})
    }
},20000)

cron.schedule('0 0 0 * * *',()=>{
    let list = fs.readJsonSync('./list.json');
    list.everyday.push(now);
    fs.writeJson('./list.json',list,(err)=>{
        if(err){console.log(`致命性错误，无法写入list,${JSON.stringify(err)}`)}
    });
    let date = getDate();
    now = [date,{"Clash.yaml": 0 ,"long": 0,"https": 0}]
})

function preLoad(){
    let today = fs.readJsonSync('./today.json');
    let list = fs.readJsonSync('./list.json');
    total = fs.readJsonSync('./total.json');
    cache = fs.readJsonSync('./cache.json');
    svgCache = fo.readFileSync('./svg.svg','utf-8');
    let date = getDate();
    if (today[0] === date) {
        now = today
    } else {
        list.everyday.push(today);
        fs.writeJson('./list.json', list, (err) => {
            console.log(`Error at launch,${JSON.stringify(err)}`)
        });
        now = [date, {"Clash.yaml": 0, "long": 0, "https": 0}]
    }
}


function getDate(){
    let date = new Date()
    let month = date.getMonth()+1
    return date.getFullYear().toString() + " " + month.toString() + " " + date.getDate().toString()
}

function add(input){
    if(focusOn.indexOf(input) !== -1){
        console.log(`Added ${input}`)
        now[1][input]++
        total[input]++
    }
}

http.createServer( function (request, response) {
        let urlDet = url.parse(request.url);
        let reqUrl = urlDet.pathname.toString().slice(1);
        let query = urlDet.query;
        if(query!==null){
            switch (query){
                case "svg":
                    let svg = svgCache;
                    for(let i=0;i<focusOn.length;i++){
                        svg = svg.replace('today@'+focusOn[i],now[1][focusOn[i]])
                        svg = svg.replace('total@'+focusOn[i],total[focusOn[i]])
                    };
                    response.writeHead(200,{'content-Type': 'image/svg+xml;charset=utf-8'});
                    response.end(svg);
                    break;
                case "count":
                    response.writeHead(200,{'content-Type': 'text/plain; charset=utf-8'});
                    response.end(JSON.stringify({total:total,today:now}));
                    break;
                default:
                    response.writeHead(404,{'content-Type': 'text/plain; charset=utf-8'});
                    response.end('不知道你想要什么');
            }
        }else if(reqUrl == 0){
            response.setHeader('Location','https://doc.opit.top')
            response.statusCode = 302;
            response.end()
        }else{
            axios("https://raw.githubusercontent.com/yu-steven/openit/main/"+reqUrl,{cache: true})
            .then(function (resp) {
                add(reqUrl)
                response.writeHead(resp.status,resp.headers)
                response.end(resp.data)
            }).catch((err)=>{
                console.log('err')
                response.writeHead(404, {'content-Type': 'text/plain; charset=utf-8'});
                response.write('没有找到你想要的资源。如果看见该错误，请尝试重新获取。');
                response.end();
            });
        }
    }
).listen(80);

